terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~>2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_app" "zero2prod" {
  spec {
    name   = "zero2prod"
    region = "nyc1"

    service {
      name            = "zero2prod"
      dockerfile_path = "Dockerfile"
      source_dir      = "."
      gitlab {
        branch         = "main"
        deploy_on_push = true
        repo           = "mshrn-rustlang-learning/zero2prod"
      }
      health_check {
        http_path = "/health_check"
      }
      http_port          = 8000
      instance_count     = 1
      instance_size_slug = "basic-xxs"
      routes {
        path = "/"
      }
      env {
        key   = "APP_DATABASE__USERNAME"
        scope = "RUN_TIME"
        value = "$${newsletter.USERNAME}"
      }
      env {
        key   = "APP_DATABASE__PASSWORD"
        scope = "RUN_TIME"
        value = "$${newsletter.PASSWORD}"
      }
      env {
        key   = "APP_DATABASE__HOST"
        scope = "RUN_TIME"
        value = "$${newsletter.HOSTNAME}"
      }
      env {
        key   = "APP_DATABASE__PORT"
        scope = "RUN_TIME"
        value = "$${newsletter.PORT}"
      }
      env {
        key   = "APP_DATABASE__DATABASE_NAME"
        scope = "RUN_TIME"
        value = "$${newsletter.DATABASE}"
      }
      env {
        key   = "APP_APPLICATION__BASE_URL"
        scope = "RUN_TIME"
        value = "$${APP_URL}"
      }
      env {
        key   = "APP_REDIS_URI"
        scope = "RUN_TIME"
        value = "$${redis.REDIS_URL}"
      }
    }
  }

}

resource "digitalocean_database_cluster" "newsletter" {
  name       = "newsletter"
  engine     = "pg"
  version    = "12"
  size       = "db-s-1vcpu-1gb"
  region     = "nyc1"
  node_count = 1
}

resource "digitalocean_database_cluster" "redis" {
  name       = "redis"
  engine     = "redis"
  version    = "6"
  size       = "db-s-1vcpu-1gb"
  region     = "nyc1"
  node_count = 1
}