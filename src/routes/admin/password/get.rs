use actix_web::{http::header::ContentType, HttpResponse};
use actix_web_flash_messages::IncomingFlashMessages;
use askama::Template;

use crate::{
    session_state::TypedSession,
    utils::{e500, see_other},
};

#[derive(Template)]
#[template(path = "admin_password.html")]
struct PasswordTemplate<'a> {
    flash_messages: &'a IncomingFlashMessages,
}
pub async fn change_password_form(
    session: TypedSession,
    flash_messages: IncomingFlashMessages,
) -> Result<HttpResponse, actix_web::Error> {
    if session.get_user_id().map_err(e500)?.is_none() {
        return Ok(see_other("/login"));
    };
    let password_template = PasswordTemplate {
        flash_messages: &flash_messages,
    };
    Ok(HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(password_template.render().unwrap()))
}
