# zero to production in Rust

## Setup
### install a linker of mold
- Following an instruction in [https://github.com/rui314/mold#How-to-use-##If-you-are-using-Rust](https://github.com/rui314/mold#:~:text=If%20you%20are%20using%20Rust), install mold via homebrew
- add this configuration file to the project

### add components
- cargo-watch
- cargo-expand
    - w/ nightly (`rustup toolchain install nightly --alow-downgrade`)
- cargo install --version="~0.6" sqlx-cli --no-default-features --features rustls,postgres
- libpq
- cargo install udeps
- cargo install bunyan
## Optional Setup
### SSH Key registration for gitlab
- make a key `ssh-keygen -t ed25519 ~/.ssh/[YOUR PREFERED FILE NAME]`
- register the key on gitlab

