use actix_web::{cookie::Cookie, http::header::ContentType, HttpResponse};
use actix_web_flash_messages::IncomingFlashMessages;
use askama::Template;

#[derive(Template)]
#[template(path = "login.html")]
struct LoginTemplate<'a> {
    flash_messages: &'a IncomingFlashMessages,
}

pub async fn login_form(flash_messages: IncomingFlashMessages) -> HttpResponse {
    let login = LoginTemplate {
        flash_messages: &flash_messages,
    };
    let mut response = HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(login.render().unwrap());

    response
        .add_removal_cookie(&Cookie::new("_flash", ""))
        .unwrap();

    response
}
