use actix_web::{http::header::LOCATION, HttpResponse};

pub fn e500<T: std::fmt::Debug + std::fmt::Display + 'static>(e: T) -> actix_web::Error {
    actix_web::error::ErrorInternalServerError(e)
}

pub fn see_other(location: &str) -> HttpResponse {
    HttpResponse::SeeOther()
        .insert_header((LOCATION, location))
        .finish()
}

pub fn e400<T: std::fmt::Debug + std::fmt::Display + 'static>(e: T) -> actix_web::Error {
    actix_web::error::ErrorBadRequest(e)
}
