# Memo
## application/x-www-form-app-urlencoded
- htmlのフォームデータを送信する際に使われるメディアタイプ(Content-Type)である。
    > the keys and values [in our form] are encoded in key-value tuples separated by ‘&’, with a ‘=’ between　the key and the value. Non-alphanumeric characters in both keys and values are percent encoded.
- メソッドとしてはPOSTを利用するものの、メタ情報としては実際に使いたいメソッドをサーバーに送る手段の一つにも利用されている。
    - POST/GETしかメソッドをサポートしていないAPIで用いられる。
    - もう一つの方法はX-HTTP-Method-Overrideリクエストヘッダである。(WebAPIの書籍での推奨はこちら)
- application/jsonというパラメーターもある。

## udeps
`cargo +nightly udeps`でCargo.tomlから不要な依存関係をリストアップしてくれる。

## Extracator
- HTTPリクエストから必要なデータを抽出するためのものである。
- actix_web::FromRequestを実装した型
- といいつつやることは`#[derive(serde::Deserialize)]`をつけるだけ。

## [formal methods](https://lamport.azurewebsites.net/tla/formal-methods-amazon.pdf)
- あとで読む

## [The Facade Pattern](https://www.techscore.com/tech/DesignPattern/Facade)
- see also https://t.co/QV79I0MDDm

## Logging in Rust
- https://www.forcia.com/blog/001605.html
### tracing:　分散トレーシング
- span: 分散トレーシングにおけるユーザーのトランザクションの完了を追う論理単位[ref](https://signoz.io/blog/distributed-tracing-span/)
- tracing::Instrumentもよくわからない。
- bunyanはnode.jsのそれ。https://qiita.com/suin/items/1f45d77672a7b6174465

## sqlx offline
- `cargo.toml.sqlx.dependencies.features.offline`
- `cargo sqlx prepare -- --lib`
- `ENV SQLX_OFFLINE true`

## digital ocean
- gitalbとの連携がめんどくさいが、WebUI上でDockerデプロイを試みると簡単にできる。CLIでの認証がどこにあるのかよくわからん。

## Wiremock
- RESTAPIのテストかなり描きやすい。
- bodyのマッチャーもある。
- モックガードを使う理由があまり理解できていないのでおいおい調べる。[ToDo]

## serde
- jsonのPascalCaseとうまく対応づけられるマクロがある。

## testの構造化
- tests/apiというディレクトリ構造にする意義をあまり理解できていない。
    >“There are a few positive side-effects to the new structure: - it is recursive.
    >If tests/api/subscriptions.rs grows too unwieldy, we can turn it into a module, with tests/api/subscriptions/helpers.rs holding subscription-specific test helpers and one or more test files focused on a >specific flow or concern; - the implementation details of our helpers function are encapsulated.”
    抜粋:
    Zero To Production In Rust
    Luca Palmieri
    この素材は著作権で保護されている可能性があります。

- test helpersをシェアできるようにしたいから？
    >“The second option takes full advantage of that each file under tests is its own executable - we can create sub-modules scoped to a single test executable!
    >Let’s create an api folder under tests, with a single main.rs file inside:”
    抜粋:
    Zero To Production In Rust
    Luca Palmieri
    この素材は著作権で保護されている可能性があります。

## Rolling update
- DBの更新方法7.6節
- alter tableコマンドを追記していくsqlxのスタイルは、変更差分をsqlxが知るため？いまひとつメリットがわからない。

## DB
### トランザクション
- 複数コマンドをまとめたものであり、トランザクション中のすべてのコマンドが成功した場合だけデータベースに対して変更を加える。
- 複数コマンド間に別処理が挟まらないことが保証される。
- see https://www.postgresql.jp/document/7.2/tutorial/tutorial-transactions.html
    > PostgreSQL ではトランザクションを構成するSQL コマンドを BEGIN と COMMIT で囲んで設定します。そうすると、このでの銀行取り引きのトランザクションは実際つぎのようになります。
    ```sql
    BEGIN;
    UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
    UPDATE branches SET balance = balance - 100.00
        WHERE name = (SELECT branch_name FROM accounts WHERE name = 'Alice');
    UPDATE accounts SET balance = balance + 100.00
        WHERE name = 'Bob';
    UPDATE branches SET balance = balance + 100.00
        WHERE name = (SELECT branch_name FROM accounts WHERE name = 'Bob');
    COMMIT;
    ```
- sqlxでは専用のAPIが用意されているので、`BEGIN`コマンドを入れる必要はない。`PgPool`の代わりに`Transaction<_, Postgres>`を渡す。このときまだCOMMIT相当の箇所は入っていない。そのため、Dropトレイトが呼ばれた際にロールバックするようになっている。

## Error
- “actix_web::Error is used to carry errors from std::error through actix_web in a convenient way.”
- オリジナルのエラーにimplさせることで、エラーを伝搬させることができる。
```rust
use actix_web::ResponseError;

pub struct YourOriginalError;
impl ReponseError for YourOriginalError {}

```
- Debug/Displayトレイトの実装が大事。Debugの場合はエラーを伝搬させるのが良い。

- thiserrorのマクロ
>“Within the context of #[derive(thiserror::Error)] we get access to other attributes to achieve the behaviour we are looking for:
>#[error(/* */)] defines the Display representation of the enum variant it is applied to. E.g. Display will return Failed to send a confirmation email. when invoked on an instance of SubscribeError::SendEmailError. You can interpolate values in the final representation - e.g. the {0} in #[error("{0}")] on top of ValidationError is referring to the wrapped String field, mimicking the syntax to access fields on tuple structs (i.e. self.0).
>#[source] is used to denote what should be returned as root cause in Error::source;
>#[from] automatically derives an implementation of From for the type it has been applied to into the top-level error type (e.g. impl From<StoreTokenError> for SubscribeError {/* */}). The field annotated with #[from] is also used as error source, saving us from having to use two annotations on the same field (e.g. #[source] #[from] reqwest::Error).”

- Error型を泥団子みたいに列挙するのは適切な構造化ではない。関数のレベルに応じた抽象化が必要である。
- ただし、内部情報を適切にデバッグするためには、どこかしらでの列挙は必要である。これをサポートするべく、`UnexpectedError(#[source] Box<dyn std::error::Error>, String)`というように、エラー型にStringの付属情報を与えることができる。
- thiserror とanuyhowの使い分けが重要。thiserrorだけでも基本良いし、コード量を減らせるが、anyhowを使った方が適切な構造化ができる感じ。contextを毎回書くのが面倒といえばそう。thiserrorの方が明確に列挙できるからライブラリとして書きやすい。ユーザーに任せずに済むが、その判断コスト高いよねって話。

|              | Internal               | At the edge   |
|--------------|------------------------|---------------|
| Control Flow | Types, methods, fields | Status codes  |
| Reporting    | Logs or traces         | Response body |

- `anyhow::Context`でいい感じにOption から anyhow::Resultに変換できる。contextとwith_contextがあるが後者は遅延評価できる。

## セキュリティ
### パスワードハッシュ
- ハッシュは非可逆変換、暗号化は可逆変換という違いがある。
    - また、写像は単射ではないので衝突の可能性がわずかではある。
- ハッシュ化においては、元の文字数によらずに固定長へと変換する。
    - 固定長へと変換することで一致検査にかかる計算が楽になる。
    - https://emn178.github.io/online-tools/sha3_512.htmlで遊んでみると良い。
    - 文字数の下限を設けると、(1文字から総当たりしていくという前提のもとで)、最低限計算に要する時間を見積もることになる。
- 写像は採用されるアルゴリズムで一意に定まるので、ハッシュをDBに保管していたとしても、典型的なパスワードであればクラックされてしまう(レインボーテーブル攻撃)。これを防ぐためにソルト(DBと同じ場所におく)、ペッパー(DBと別の場所におく)というものも併用されることが多い。これらをおくことでそれぞれのハッシュ化が異なるので、ユーザーごとに
- OWASPによる、ハッシュアルゴリズムの選定方針: [safe password storage](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html)によると、argon2idを使うのが原則。
- 実装において、アルゴリズムやアルゴリズムのパラメーターも保存しなければハッシュを復元できないが、どこまでハードコードしておくかを考えるのは煩雑なので、これらとハッシュを合わせてハッシュとして保存するPHCStringというものがある。

### ブロッキング
- 1ms以上かかる処理については、ポーリングがうまく機能しない(並列処理がブロックされる)ので、専用のスレッドプールにためるようにする必要がある。
    - 10µs~100µsで応答できるのが通常のポーリング対象
    - 今の文脈においてはargon2idを使ったハッシュ計算が重い処理ということになる。
    - bunyanではelapsed timeが見れるようになっている
        ```sh
        $ TEST_LOG=true cargo test --quiet --release newsletters_are_delivered | grep "VERIFY PASSWORD" | bunyan
        [2022-12-26T07:02:15.833Z]  INFO: test/24398 on mshrn-macmini.local: [VERIFY PASSWORD HASH - START] (file=src/routes/newsletters.rs,http.client_ip=127.0.0.1,http.flavor=1.1,http.host=localhost:53430,http.method=POST,http.route=/newsletters,http.scheme=http,http.target=/newsletters,http.user_agent="",line=186,otel.kind=server,otel.name="HTTP POST /newsletters",request_id=a5dc40ef-766b-4d6d-9acc-e3049ae26bbd,target=zero2prod::routes::newsletters,username=b5015b18-368a-4fe0-92e4-f0898bcb5670)
        [2022-12-26T07:02:15.842Z]  INFO: test/24398 on mshrn-macmini.local: [VERIFY PASSWORD HASH - END] (elapsed_milliseconds=8,file=src/routes/newsletters.rs,http.client_ip=127.0.0.1,http.flavor=1.1,http.host=localhost:53430,http.method=POST,http.route=/newsletters,http.scheme=http,http.target=/newsletters,http.user_agent="",line=186,otel.kind=server,otel.name="HTTP POST /newsletters",request_id=a5dc40ef-766b-4d6d-9acc-e3049ae26bbd,target=zero2prod::routes::newsletters,username=b5015b18-368a-4fe0-92e4-f0898bcb5670)
        ```

### user enumeration attack
- ハッシュ計算に時間がかかってしまうと、ユーザーが存在する場合と存在しない場合とでアクセス拒否を返すまでの時間が異なってしまう。この時間差を利用することでユーザー名を取得することができてしまう。
- この脆弱性をユーザー列挙攻撃と呼ぶ。

### XSS対策
- クロスサイトスクリプティング攻撃(XSS)を防ぐためにクエリパラメーターを制限する必要がある。(e.g.“http://localhost:8000/login?error=href...”みたいにリンクを埋め込む)
- まず第一に意図せぬHTMLタグの挿入を防ぐために、<>などをエンコーディングする。
    According to OWASP’s guidelines, we must HTML entity-encode the untrusted input - i.e.:
    convert & to &amp;
    convert < to &lt;
    convert > to &gt;
    convert " to &quot;
    convert ' to &#x27;
    convert / to &#x2F.”
- 次に、正しい発行元からのクエリーパラメーターであることを保証するために、メッセージ認証(MAC)を用いる。
- ハッシュ化された認証を特にHMACと呼ぶ。
- このハッシュをふすためにHTMLではタグディレクティブを用いる。
- そもそもクッキーを使えばインジェクションのハードルをさらにあげることができる。
- クッキーも平文で通信すると脆弱であるから、MACすることが推奨されている。(signed cookie)

### session fixation attacks
- see https://www.ubsecure.jp/blog/session_fixation

## the messages framework
- https://docs.djangoproject.com/en/3.2/ref/contrib/messages/#module-django.contrib.messages
- ログイン前後でセッションIDが変化しないと、攻撃者によってセッションIDを固定されてしまい、なりすましの恐れがある。

## テンプレートエンジン
- askamaを使っている。
    - デフォルトでOWASPに則ってエスケープされるので、フィルターを適宜入れる必要がある。
        - https://djc.github.io/askama/template_syntax.html#html-escaping
        - https://djc.github.io/askama/filters.html


## terraform 
- [best practice](https://cloud.google.com/docs/terraform/best-practices-for-terraform?hl=ja)
- [digitalocean tutorials](https://www.digitalocean.com/community/tutorial_series/how-to-manage-infrastructure-with-terraform)
- [digitalocean references](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/app)
- [tfstateの意義](https://chroju.dev/blog/terraform_state_introduction)
- [tfstate/backendのgitlab管理](https://kazuhira-r.hatenablog.com/entry/2020/06/13/202230)


## リトライセーフ
- 周知の定義がない。
- この本では、idempotentなものを意味し、複数回リトライしているかどうかをAPIを叩いた人間には区別できないものと扱う。
- idempotency keyを使う。ステートレスにもできるし実装はシンプルにできるけど、サービスレベルの要求が厳しいとステートフルにする必要がある。

## 復帰
- backward recovery: 処理を遡及的にキャンセルする。(ロールバック)
- forward recovery: 処理を上書きする。　(ロールフォワード)
    - passive: 同期ワークフローにてリトライを何度か繰り返す
    - active: 非同期に切り出してリトライさせる。

## row leve locks in DB
- DBにおけるロック操作がいくつかある。
    ```
    SELECT (newsletter_issue_id, subscriber_email)
    FROM issue_delivery_queue
    FOR UPDATE
    SKIP LOCKED
    LIMIT 1
    ```
    - 取得したカラムに対するロックは`FOR UPDATE`で取得する。
    - 他の操作からロックされているカラムを含む場合、`SKIP LOCKED`でそれらをスキップできる。

## コード改変の明示漏れ
### 11.10.1 Distributed Transactions以降
- `src/startup.rs`の`get_connection_pool`まわり: Resultを返すのではなくなる、非同期でもない。それにともなって呼び出し箇所でも`await`, `expect`が不要になる。
    ```rust
    pub fn get_connection_pool(configuration: &DatabaseSettings) -> PgPool {
        PgPoolOptions::new()
            .acquire_timeout(std::time::Duration::from_secs(2))
            .connect_lazy_with(configuration.with_db())
    }
    ```
- `src/routes/admin/newsletter/post.rs`の`success_message`まわり
    ```rs
    fn success_message() -> FlashMessage {
        FlashMessage::info(
            "The newsletter issue has been accepted - \
            emails will go out shortly.",
        )
    }
    ```