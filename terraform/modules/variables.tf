variable "do_token" {
  description = "Your digitalOcean personal access token. This should be set using like terraform apply -var=\"do_token=...\" "
  default     = ""
}