use crate::authentication::{validate_credentials, AuthError, Credentials, UserId};
use crate::routes::admin::dashboard::get_username;
use crate::utils::{e500, see_other};
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::FlashMessage;
use secrecy::{ExposeSecret, Secret};
use sqlx::PgPool;

#[derive(serde::Deserialize)]
pub struct FormData {
    current_password: Secret<String>,
    new_password: Secret<String>,
    new_password_check: Secret<String>,
}

pub async fn change_password(
    form: web::Form<FormData>,
    pool: web::Data<PgPool>,
    user_id: web::ReqData<UserId>,
) -> Result<HttpResponse, actix_web::Error> {
    let user_id = user_id.into_inner();

    if let Some(result) = validate_new_password(form.0.new_password.expose_secret()) {
        FlashMessage::error(&result).send();
        return Ok(see_other("/admin/password"));
    }

    if form.new_password.expose_secret() != form.new_password_check.expose_secret() {
        FlashMessage::error(
            "You entered two different new passwords - the field values must match.",
        )
        .send();
        return Ok(see_other("/admin/password"));
    }

    let username = get_username(*user_id, &pool).await.map_err(e500)?;

    let credentials = Credentials {
        username,
        password: form.0.current_password,
    };

    if let Err(e) = validate_credentials(credentials, &pool).await {
        return match e {
            AuthError::InvalidCredentials(_) => {
                FlashMessage::error("The current password is incorrect.").send();
                Ok(see_other("/admin/password"))
            }
            AuthError::UnexpectedError(_) => Err(e500(e)),
        };
    }

    crate::authentication::change_password(*user_id, form.0.new_password, &pool)
        .await
        .map_err(e500)?;
    FlashMessage::error("Your password has been changed.").send();
    Ok(see_other("/admin/password"))
}

fn validate_new_password(new_password: &str) -> Option<String> {
    if new_password.len() < 13 || new_password.len() > 127 {
        Some(
            "The new password should be longer than 12 characters but shorter than 128 characters."
                .to_string(),
        )
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::validate_new_password;
    use fake::{faker::internet::en::Password, Fake};

    #[derive(Debug, Clone)]
    struct TooShortPasswordFixture(pub String);
    impl quickcheck::Arbitrary for TooShortPasswordFixture {
        fn arbitrary<G: quickcheck::Gen>(g: &mut G) -> Self {
            let password = Password(1..13).fake_with_rng(g);
            Self(password)
        }
    }
    #[quickcheck_macros::quickcheck]
    fn password_validator_prohibits_password_less_than_12_characters(
        password: TooShortPasswordFixture,
    ) -> bool {
        dbg!(&password.0);

        match validate_new_password(&password.0) {
            Some(_) => true,
            None => false,
        }
    }

    #[derive(Debug, Clone)]
    struct TooLongPasswordFixture(pub String);
    impl quickcheck::Arbitrary for TooLongPasswordFixture {
        fn arbitrary<G: quickcheck::Gen>(g: &mut G) -> Self {
            let password = Password(128..256).fake_with_rng(g);
            Self(password)
        }
    }
    #[quickcheck_macros::quickcheck]
    fn password_validator_prohibits_password_more_than_127_characters(
        password: TooLongPasswordFixture,
    ) -> bool {
        dbg!(&password.0);

        match validate_new_password(&password.0) {
            Some(_) => true,
            None => false,
        }
    }
}
