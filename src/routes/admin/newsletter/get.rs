use actix_web::http::header::ContentType;
use actix_web::HttpResponse;
use actix_web_flash_messages::IncomingFlashMessages;
use askama::Template;

#[derive(Template)]
#[template(path = "admin_newsletter.html")]
struct NewsletterTemplate<'a> {
    flash_messages: &'a IncomingFlashMessages,
    idempotency_key: &'a String,
}
pub async fn publish_newsletter_form(
    flash_messages: IncomingFlashMessages,
) -> Result<HttpResponse, actix_web::Error> {
    let template = NewsletterTemplate {
        flash_messages: &flash_messages,
        idempotency_key: &uuid::Uuid::new_v4().to_string(),
    };

    Ok(HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(template.render().unwrap()))
}
